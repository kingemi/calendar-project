package Calendar;

import com.mindfusion.common.DateTime;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * userProfile contains information about the user, and can be used to save data
 */
public class userProfile {
    private String name;
    private int age;
    private String phoneNumber;
    private int saveCode;
    private boolean transportation;

    /**
     * run to create a new user profile
     * @return the created profile
     */
    public userProfile createProfile(){

        createName();
        createAge();
        createPhone();
        saveCode = createSave();
        boolean transportation = false;
        return this;
    }

    private void createName(){
        String st;
        JFrame nameFrame = new JFrame("New User: Name");
        //Setting the width and height of frame
        nameFrame.setSize(350, 350);
        JPanel panel = new JPanel();
        nameFrame.add(panel);
        nameFrame.setVisible(true);

        JLabel namePrompt = new JLabel("Input Name: ");
        //todo: set position and size of label, like below
        //namePrompt.setBounds(10,20,80,25);
        panel.add(namePrompt);

        JTextField namePromptText = new JTextField(20);
        //todo: set position and size of text field, like below
        //eventNameText.setBounds(140,20,165,25);
        panel.add(namePromptText);

        JButton ok = new JButton("OK");
        //todo: set position and size of button, like below
        //ok.setBounds(140,20,165,25);
        panel.add(ok);

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == ok) {
                    if(checkName(namePromptText.getText())){
                        setName(namePromptText.getText());
                        nameFrame.setVisible(false);
                    }
                    else{
                        //todo: write what happens if user input is invalid
                    }
                }
            }
        });
    }

    private void createAge(){
        JFrame ageFrame = new JFrame("New User: Age");
        //Setting the width and height of frame
        ageFrame.setSize(350, 350);
        JPanel panel = new JPanel();
        ageFrame.add(panel);
        ageFrame.setVisible(true);

        JLabel agePrompt = new JLabel("Input Name: ");
        //todo: set position and size of label, like below
        //namePrompt.setBounds(10,20,80,25);
        panel.add(agePrompt);

        JTextField agePromptText = new JTextField(20);
        //todo: set position and size of text field, like below
        //eventNameText.setBounds(140,20,165,25);
        panel.add(agePromptText);

        JButton ok = new JButton("OK");
        //todo: set position and size of button, like below
        //ok.setBounds(140,20,165,25);
        panel.add(ok);

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == ok) {
                    if(checkNum(agePromptText.getText())){
                        setAge(agePromptText.getText());
                        ageFrame.setVisible(false);
                    }
                    else{
                        //todo: write what happens if user input is invalid
                    }
                }
            }
        });
    }

    private void createPhone(){
        JFrame phoneFrame = new JFrame("New User: Age");
        //Setting the width and height of frame
        phoneFrame.setSize(350, 350);
        JPanel panel = new JPanel();
        phoneFrame.add(panel);
        phoneFrame.setVisible(true);

        JLabel phonePrompt = new JLabel("Input Name: ");
        //todo: set position and size of label, like below
        //namePrompt.setBounds(10,20,80,25);
        panel.add(phonePrompt);

        JTextField phonePromptText = new JTextField(20);
        //todo: set position and size of text field, like below
        //eventNameText.setBounds(140,20,165,25);
        panel.add(phonePromptText);

        JButton ok = new JButton("OK");
        //todo: set position and size of button, like below
        //ok.setBounds(140,20,165,25);
        panel.add(ok);

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == ok) {
                    if(checkNum(phonePromptText.getText())){
                        setNumber(phonePromptText.getText());
                        phoneFrame.setVisible(false);
                    }
                    else{
                        //todo: write what happens if user input is invalid
                    }
                }
            }
        });
    }
    //function gives profile unique save code to identify by
    //returns first unused save code
    //todo: implement save system using save code
    private int createSave(){
        return 0;
    }

    // Formatting phone number
    private String fmtPhoneNumber(){
        String str = "("+ phoneNumber.substring(0,3)+")"+ phoneNumber.substring(3,6)+"-"+phoneNumber.substring(6);
        return str;
    }

    // prints name, age, phone number, and transportation facility
    private void printStatement(){
        System.out.println("\tUSER PROFILE\t");
        System.out.println("Name: " +getName());
        System.out.println("Age: " +getAge());
        System.out.println("Number: " +fmtPhoneNumber());
    }

    //sets the phone number to n, but only if the String length is exactly ten
    private String setNumber(String n){
        if(n.length()==10){
            return n;
        }
        return "9999999999";
    }
    private void setTransport(boolean transport) {
        if (transport) {
            transportation = true;
            System.out.println("Has transportation");
        } else {
            transportation = false;
            System.out.println("Does not have transportation");
        }
    }
    //checks that the input string is an int
    private boolean checkNum(String s){
        //todo: write this code
        return true;
    }
    //checks that the input name contains only valid characters
    private boolean checkName(String s){
        //todo: write this code
        return true;
    }

    private void setName(String s){
        name = s;
    }
    private void setAge(String s){
        age = Integer.parseInt(s);
    }

    /**
     * get user's name
     * @return user's name
     */
    public String getName() {
        return name;
    }

    /**
     * get user's age
     * @return user's age
     */
    public int getAge() {
        return age;
    }

    /**
     * get user's phone number
     * @return user's phone nuber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * get the profile save code
     * @return save code
     */
    public int getCode(){
        return saveCode;
    }
}

